<?php

namespace Srhinow\CeArticleList\Service;

use Contao\CoreBundle\Routing\ScopeMatcher;
use Symfony\Component\HttpFoundation\RequestStack;

class ModeService
{
    public function __construct(
        private readonly RequestStack $requestStack,
        private readonly ScopeMatcher $scopeMatcher,
    ) {
    }

    public function isBackend(): bool
    {
        return $this->scopeMatcher->isBackendRequest($this->requestStack->getCurrentRequest());
    }

    public static function isBackendStatic(): bool
    {
        return self::isBackend();
    }

    public function isFrontend()
    {
        return $this->scopeMatcher->isFrontendRequest($this->requestStack->getCurrentRequest());
    }

    public function isFrontendStatic(): bool
    {
        return self::isFrontend();
    }

    public function getMode(): bool
    {
        if(!$this->scopeMatcher->isContaoRequest($this->requestStack->getCurrentRequest())){
            return '';
        }
        
        return $this->isBackend() ? 'BE' : 'FE';
    }
}