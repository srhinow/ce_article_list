<?php

/**
 * @copyright  Lingo4you 2018
 * @author     Mario Müller <http://www.lingo4u.de/>
 * @package    ArticleList
 * @license    http://opensource.org/licenses/lgpl-3.0.html
 */


/**
 * Content elements
 */
$GLOBALS['TL_CTE']['includes']['article_list'] = 'Srhinow\CeArticleList\ArticleList';
$GLOBALS['TL_CTE']['includes']['page_list'] = 'Srhinow\CeArticleList\PageList';
